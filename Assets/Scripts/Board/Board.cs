﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Board : MonoBehaviour
{
    public static Board instance = null;
    
    private static int m_sizeBoard = 9;
    public static int GetSizeBoard
    {
        get { return m_sizeBoard; }
    }
    
    [SerializeField] private GameObject m_cellPrefab;

    
    private static Collider2D[,] m_cells;
    private static List<Vector2Int> m_clearPositions = new List<Vector2Int>();

    public static int GetClearPositionCount
    {
        get { return m_clearPositions.Count; }
    }
    

    private void Awake()
    {
        //////////////////////// Singleton release //////////////////////////////

        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            Destroy(gameObject);
        }
        
        //////////////////////// Create Board ////////////////////////////// 
        
        m_cells = new Collider2D[m_sizeBoard, m_sizeBoard];
        
        for (int y = 0; y < m_sizeBoard; y++)
        {
            for (int x = 0; x < m_sizeBoard; x++)
            {
                Vector2Int pos = new Vector2Int(x, y);
                var cell = Instantiate(m_cellPrefab, (Vector2) pos, Quaternion.identity, gameObject.transform);
                var collider = cell.GetComponent<Collider2D>();
                m_cells[x, y] = collider;
                m_clearPositions.Add(pos);
            }
        }
    }

    public static void DisableCell(Vector2Int position)
    {
        m_cells[position.x, position.y].enabled = false;
    }
    
    public static void EnableCell(Vector2Int position)
    {
        m_cells[position.x, position.y].enabled = true;
    }
    
    //////////////////////// Get an empty cell from an array of empty cells ////////////////////////////// 

    public static TruePosition<bool, Vector2Int> GetRandomClearPosition()
    {
        var position = new TruePosition<bool, Vector2Int>();
        
        if (m_clearPositions.Count <= 0)
        {
            position.isTrue = false;
            return position;
        }
        else
        {
            position.isTrue = true;
        }
        
        int random = Random.Range(0, m_clearPositions.Count);

        position.position = m_clearPositions[random];
        return position;
    }
    
    //////////////////////// Remove empty cell from empty cell array ////////////////////////////// 

    public static void RemoveClearPosition(Vector2Int position)
    {
        m_clearPositions.Remove(position);
    }

    public static void AddClearPosition(Vector2Int position)
    {
        m_clearPositions.Add(position);
    }
    
    //////////////////////// Swap empty cells ////////////////////////////// 

    public static void ChangeClearPosition(Vector2Int newPosition, Vector2Int oldPosition)
    {
        RemoveClearPosition(newPosition);
        DisableCell(newPosition);
        
        AddClearPosition(oldPosition);
        EnableCell(oldPosition);
    }

    public static void WriteNumber(Vector2Int pos, int number)
    {
        m_cells[pos.x, pos.y].GetComponentInChildren<TextMeshProUGUI>().text = "" + number;
    }
}

public class TruePosition<T, T1>
{
    public bool isTrue;
    public Vector2Int position;
}
