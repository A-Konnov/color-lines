﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_yourScoreTxtUI;
    [SerializeField] private TextMeshProUGUI m_recordTxtUI;
    [SerializeField] private Score m_score;

    private void Start()
    {
        Balls.EndGameEvent.AddListener(Open);
    }

    private void OnDestroy()
    {
        Balls.EndGameEvent.RemoveListener(Open);
    }

    public void Open()
    {
        m_score.SaveRecord();
        GetComponent<Animator>().SetTrigger("Open");

        m_yourScoreTxtUI.text = "your score: " + m_score.ScoreChange;
        m_recordTxtUI.text = "record: " + PlayerPrefs.GetInt("Record");
    }

    public void Restart()
    {
        SceneManager.LoadScene(1);
    }
}
