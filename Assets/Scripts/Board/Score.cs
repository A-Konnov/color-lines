﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Score : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_scoreTxt;
    private int m_score;
    public int ScoreChange
    {
        get { return m_score; }
        set
        {
            m_score = m_score + value;
            m_scoreTxt.text = "" + m_score;
        }
    }

    private void Start()
    {
        ScoreChange = 0;
    }

    public void SaveRecord()
    {
        var record = PlayerPrefs.GetInt("Record");
        if (m_score > record)
        {
            PlayerPrefs.SetInt("Record", m_score);
        }
    }

}
