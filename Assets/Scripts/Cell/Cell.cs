﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    private void OnMouseDown()
    {
        if (Balls.BallSelected == null)
        {
            return;
        }

        int posX = (int) transform.position.x;
        int posY = (int) transform.position.y;
        
        Balls.MoveBall(new Vector2Int(posX, posY));
    }
}
