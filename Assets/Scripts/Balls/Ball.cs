﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] private EColor m_color;
    [SerializeField] private Sprite[] m_sprites;
    private Animator m_animator;
    private SpriteRenderer m_sr;
    
    private Vector2Int m_position;
    public Vector2Int Position
    {
        get { return m_position; }
        set { m_position = value; }
    }

    private void Start()
    {
        m_sr = gameObject.GetComponentInChildren<SpriteRenderer>();
        m_animator = GetComponent<Animator>();
        AssignColor();
        m_animator.SetTrigger("Create");
    }

    public void AnimDestroy()
    {
        m_animator.SetTrigger("Destroy");
        Board.AddClearPosition(m_position);
        Board.EnableCell(m_position);
        Balls.BallWriteOut(m_position);
        Destroy(gameObject, 0.3f);
    }

    public EColor Color
    {
        get { return m_color; }
        set { m_color = value; }
    }

    private void AssignColor()
    {
        int colorNumber = Random.Range(0, (int)EColor.Length);
        m_color = (EColor)colorNumber;
        m_sr.sprite = m_sprites[colorNumber];
    }
    
    //////////////////////// Ball release //////////////////////////////

    private void OnMouseDown()
    {
        Ball ballSelected = Balls.BallSelected;
        
        if (ballSelected == this)
        {
            m_animator.SetBool("Select", false);
            Balls.BallSelected = null;
        }
        else
        {
            if (ballSelected != null)
            {
                ballSelected.GetComponent<Animator>().SetBool("Select", false);
            }

            m_animator.SetBool("Select", true);
            Balls.BallSelected = this;
        }
    }

    public void Deselect()
    {
        m_animator.SetBool("Select", false);
        Balls.BallSelected = null;
    }
}
