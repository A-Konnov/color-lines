﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using UnityEngine.Events;

public class Balls : MonoBehaviour
{
    public static Balls instance = null;
    public static UnityEvent EndGameEvent = new UnityEvent();

    private static List<Ball> m_previewBalls = new List<Ball>();
    private static Ball[,] m_balls;
    private static Ball m_ballSelected;
    private static BallsRemove m_br;
    private static FindingWay m_fw;

    public static Ball BallSelected
    {
        get { return m_ballSelected; }
        set { m_ballSelected = value; }
    }

    public static Ball GetBall(Vector2Int position)
    {
        return m_balls[position.x, position.y];
    }


    private static GameObject m_ball;
    [SerializeField] private GameObject m_ballPrefab;
    [SerializeField] private int m_startBalls = 5;

    private void Awake()
    {
        //////////////////////// Singleton release //////////////////////////////

        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            Destroy(gameObject);
        }
    }

    //////////////////////// Create starting balls and previews //////////////////////////////

    private void Start()
    {
        m_br = gameObject.GetComponent<BallsRemove>();
        m_fw = gameObject.GetComponent<FindingWay>();
        m_fw.Generate();

        m_ball = m_ballPrefab;

        m_balls = new Ball[Board.GetSizeBoard, Board.GetSizeBoard];

        for (int i = 0; i < m_startBalls; i++)
        {
            CreatePreviewBall();
        }

        ActivatePreviewBalls();
        CreatPreviewBallPerTurn();
    }

    //////////////////////// Create a preview of the ball //////////////////////////////

    private static void CreatePreviewBall()
    {
        var truePosition = Board.GetRandomClearPosition();
        if (!truePosition.isTrue)
        {
            return;
        }

        Vector2Int position = truePosition.position;
        GameObject ballObject = Instantiate(m_ball, (Vector2)position, Quaternion.identity);
        ballObject.transform.localScale = new Vector3(0.3f, 0.3f, 1);

        Ball ball = ballObject.GetComponent<Ball>();
        ball.Position = position;
        m_previewBalls.Add(ball);
        Board.RemoveClearPosition(position);
    }

    private static void CreatPreviewBallPerTurn()
    {
        for (int i = 0; i < 3; i++)
        {
            CreatePreviewBall();
        }
    }

    //////////////////////// Activate the balls from the preview //////////////////////////////

    private static void ActivatePreviewBalls()
    {
        foreach (var ball in m_previewBalls)
        {
            ball.transform.localScale = Vector3.one;
            ball.GetComponent<Collider2D>().enabled = true;
            ball.GetComponent<Animator>().enabled = true;

            Vector2Int position = ball.Position;
            BallWrite(ball, position);
            Board.DisableCell(position);

            m_fw.AddBlock(position);
            m_br.LineCheck(ball);
        }

        m_previewBalls.Clear();
    }

    private static void ActivatePreviewBalls(Vector2Int position)
    {
        foreach (var prevBall in m_previewBalls)
        {
            if (position == prevBall.Position)
            {
                m_previewBalls.Remove(prevBall);
                Destroy(prevBall.gameObject);
                CreatePreviewBall();
                break;
            }
        }

        ActivatePreviewBalls();
    }

    //////////////////////// Write the ball in an array of balls //////////////////////////////

    public static void BallWrite(Ball ball, Vector2Int position)
    {
        m_balls[position.x, position.y] = ball;
    }

    public static void BallWriteOut(Vector2Int position)
    {
        m_balls[position.x, position.y] = null;
    }

    //////////////////////// Move the ball //////////////////////////////

    public static void MoveBall(Vector2Int newPosition)
    {
        Vector2Int oldPosition = m_ballSelected.Position;

        var result = m_fw.Find(oldPosition, newPosition);
        if (!result)
        {
            m_ballSelected.Deselect();
            return;
        }

        m_balls[oldPosition.x, oldPosition.y] = null;
        m_balls[newPosition.x, newPosition.y] = m_ballSelected;

        m_ballSelected.GetComponent<Animator>().SetBool("Select", false);
        Board.ChangeClearPosition(newPosition, oldPosition);
        m_fw.AddBlock(newPosition);
        m_fw.RemoveBlock(oldPosition);
        m_ballSelected.Position = newPosition;

        ////move
        //m_ballSelected.transform.position = (Vector2)newPosition;
        ////

        List<Vector2Int> path = m_fw.GeneratePath(oldPosition, newPosition);
        Sequence sequence = DOTween.Sequence();
        for (int i = path.Count - 1; i >= 0; i--)
        {
            Vector3 newPos = new Vector3(path[i].x, path[i].y, 0f);
            sequence.Append(m_ballSelected.transform.DOMove(newPos, 0.2f));
        }
        sequence.AppendCallback(() => AfterMove(newPosition));
        sequence.Play();

        
    }

    private static void AfterMove(Vector2Int newPosition)
    {
        m_br.LineCheck(m_ballSelected);

        m_ballSelected = null;

        ActivatePreviewBalls(newPosition);

        CreatPreviewBallPerTurn();

        if (CheckEndGame())
        {
            EndGameEvent.Invoke();
        }
    }

    //////////////////////// Check the end of the game //////////////////////////////

    private static bool CheckEndGame()
    {
        var clearCells = Board.GetClearPositionCount + m_previewBalls.Count;
        if (clearCells <= 0)
        {
            return true;
        }

        return false;
    }
}