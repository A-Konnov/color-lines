﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallsRemove : MonoBehaviour
{
    private List<Ball> m_identicalBalls = new List<Ball>();
    private Vector2Int m_firstPosition;
    private EColor m_firstColor;
    private FindingWay m_fw;
    private Score m_score;

    private bool m_isLineCheck = false;

    private void Start()
    {
        m_fw = gameObject.GetComponent<FindingWay>();
        m_score = gameObject.GetComponent<Score>();
    }

    public void LineCheck(Ball firstBall)
    {
        m_firstPosition = firstBall.Position;
        m_firstColor = firstBall.Color;

        LineCheckHorizontal(m_firstPosition);
        LineCheckVertical(m_firstPosition);
        LineCheckDiagonalTop(m_firstPosition);
        LineCheckDiagonalBottom(m_firstPosition);


        if (m_isLineCheck)
        {
            m_fw.RemoveBlock(firstBall.Position);
            firstBall.AnimDestroy();
            m_isLineCheck = false;
        }
    }


    private void RemoveLine()
    {
        if (m_identicalBalls.Count >= 4)
        {
            m_isLineCheck = true;
            foreach (var ball in m_identicalBalls)
            {
                m_fw.RemoveBlock(ball.Position);
                ball.AnimDestroy();
            }

            m_score.ScoreChange = m_identicalBalls.Count + 1;
        }
        
        m_identicalBalls.Clear();
    }

    private bool IsBallMatches(int x, int y)
    {
        Vector2Int position = new Vector2Int(x, y);
        Ball checkBall = Balls.GetBall(position);

        if (checkBall != null)
        {
            if (checkBall.Color == m_firstColor)
            {
                m_identicalBalls.Add(checkBall);
                return true;
            }
        }
        return false;
    }

    private void LineCheckHorizontal(Vector2Int position)
    {
        int x = position.x - 1;
        while (x >= 0)
        {
            if (!IsBallMatches(x, position.y))
            {
                break;
            }
            x--;
        }

        x = position.x + 1;
        while (x < 9)
        {
            if (!IsBallMatches(x, position.y))
            {
                break;
            }
            x++;
        }

        RemoveLine();
    }

    private void LineCheckVertical(Vector2Int position)
    {
        int y = position.y - 1;
        while (y >= 0)
        {
            if (!IsBallMatches(position.x, y))
            {
                break;
            }

            y--;
        }

        y = position.y + 1;
        while (y < 9)
        {
            if (!IsBallMatches(position.x, y))
            {
                break;
            }
            y++;
        }

        RemoveLine();
    }

    private void LineCheckDiagonalTop(Vector2Int position)
    {
        int x = position.x - 1;
        int y = position.y - 1;
        while (x >= 0 && y >= 0)
        {
            if (!IsBallMatches(x, y))
            {
                break;
            }

            x--;
            y--;
        }

        x = position.x + 1;
        y = position.y + 1;
        while (x < 9 && y < 9)
        {
            if (!IsBallMatches(x, y))
            {
                break;
            }

            x++;
            y++;
        }

        RemoveLine();
    }

    private void LineCheckDiagonalBottom(Vector2Int position)
    {
        int x = position.x + 1;
        int y = position.y - 1;
        while (x < 9 && y >= 0)
        {
            if (!IsBallMatches(x, y))
            {
                break;
            }

            x++;
            y--;
        }

        x = position.x - 1;
        y = position.y + 1;
        while (x >= 0 && y < 9)
        {
            if (!IsBallMatches(x, y))
            {
                break;
            }

            x--;
            y++;
        }

        RemoveLine();
    }
}