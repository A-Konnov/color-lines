﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindingWay : MonoBehaviour
{
    private int[,] m_board;
    private int[,] m_curBoard;
    private int m_size;
    private List<Vector2Int> m_recordedCells;
    private List<Vector2Int> m_path;

    public void Generate()
    {
        m_recordedCells = new List<Vector2Int>();
        m_path = new List<Vector2Int>();

        m_size = Board.GetSizeBoard + 2;
        m_board = new int[m_size, m_size];
        for (int y = 0; y < m_size; y++)
        {
            for (int x = 0; x < m_size; x++)
            {
                m_board[x, y] = 0;
            }
        }

        for (int x = 0; x < m_size; x++)
        {
            m_board[x, 0] = -1;
            m_board[x, m_size - 1] = -1;
        }

        for (int y = 0; y < m_size; y++)
        {
            m_board[0, y] = -1;
            m_board[m_size - 1, y] = -1;
        }
    }

    public void AddBlock(Vector2Int pos)
    {
        m_board[pos.x + 1, pos.y + 1] = -1;
        Board.WriteNumber(pos, -1);
    }

    public void RemoveBlock(Vector2Int pos)
    {
        m_board[pos.x + 1, pos.y + 1] = 0;
        Board.WriteNumber(pos, 0);
    }

    public bool Find(Vector2Int startPosition, Vector2Int endPosition)
    {
        m_curBoard = (int[,]) m_board.Clone();
        
        endPosition = new Vector2Int(endPosition.x + 1, endPosition.y + 1);
        startPosition = new Vector2Int(startPosition.x + 1, startPosition.y + 1);
        
        m_curBoard[startPosition.x, startPosition.y] = 1;
        
        m_recordedCells.Clear();
        m_recordedCells.Add(startPosition);
        
        bool find = false;

        while (find == false)
        {
            if (m_recordedCells.Count == 0)
            {
                return false;
            }
            
            List<Vector2Int> curRecordedCells = new List<Vector2Int>(m_recordedCells);
            m_recordedCells.Clear();
            
            foreach (var curRecordCell in curRecordedCells)
            {
                if (curRecordCell == endPosition)
                {
                    find = true;
                    break;
                }
                
                AddIndex(curRecordCell, m_curBoard[curRecordCell.x, curRecordCell.y]);
            }
        }

        return true;
    }

    private void AddIndex(Vector2Int pos, int index)
    {
        int writeIndex = index + 1;
        
        if (m_curBoard[pos.x, pos.y - 1] == 0)
        {
            m_curBoard[pos.x, pos.y - 1] = writeIndex;
            m_recordedCells.Add(new Vector2Int(pos.x, pos.y - 1));
            Board.WriteNumber(new Vector2Int(pos.x - 1, pos.y - 2), writeIndex);
        }
        
        if (m_curBoard[pos.x, pos.y + 1] == 0)
        {
            m_curBoard[pos.x, pos.y + 1] = writeIndex;
            m_recordedCells.Add(new Vector2Int(pos.x, pos.y + 1));
            Board.WriteNumber(new Vector2Int(pos.x - 1, pos.y), writeIndex);
        }
        
        if (m_curBoard[pos.x - 1, pos.y] == 0)
        {
            m_curBoard[pos.x - 1, pos.y] = writeIndex;
            m_recordedCells.Add(new Vector2Int(pos.x - 1, pos.y));
            Board.WriteNumber(new Vector2Int(pos.x - 2, pos.y - 1), writeIndex);
        }
        
        if (m_curBoard[pos.x + 1, pos.y] == 0)
        {
            m_curBoard[pos.x + 1, pos.y] = writeIndex;
            m_recordedCells.Add(new Vector2Int(pos.x + 1, pos.y));
            Board.WriteNumber(new Vector2Int(pos.x, pos.y - 1), writeIndex);
        }
    }

    public void RewriteIndex()
    {
        for (int y = 1; y < m_size - 1; y++)
        {
            for (int x = 1; x < m_size - 1; x++)
            {
                Board.WriteNumber(new Vector2Int(x - 1, y - 1), m_board[x,y]);
            }
        }
    }

    public List<Vector2Int> GeneratePath(Vector2Int startPosition, Vector2Int endPosition)
    {
        m_path.Clear();
        m_path.Add(endPosition);

        bool end = false;

        while (end == false)
        {
            Vector2Int step = new Vector2Int(m_path[m_path.Count - 1].x + 1, m_path[m_path.Count - 1].y + 1); //position in grid
            step = GetStep(step); //position in game

            if (step == startPosition)
            {
                end = true;
                break;
            }

            m_path.Add(step);
        }

        /////Debug
        foreach (var path in m_path)
        {
            Debug.Log(path);
        }

        return m_path;
    }

    private Vector2Int GetStep(Vector2Int pos)
    {
        Vector2Int step = pos;
        int minStep = m_curBoard[pos.x, pos.y];

        if (m_curBoard[pos.x, pos.y - 1] < minStep && m_curBoard[pos.x, pos.y - 1] > 0)
        {
            minStep = m_curBoard[pos.x, pos.y - 1];
            step = new Vector2Int(pos.x - 1, pos.y - 2);
        }

        if (m_curBoard[pos.x, pos.y + 1] < minStep && m_curBoard[pos.x, pos.y + 1] > 0)
        {
            minStep = m_curBoard[pos.x, pos.y + 1];
            step = new Vector2Int(pos.x - 1, pos.y);
        }

        if (m_curBoard[pos.x - 1, pos.y] < minStep && m_curBoard[pos.x - 1, pos.y] > 0)
        {
            minStep = m_curBoard[pos.x - 1, pos.y];
            step = new Vector2Int(pos.x - 2, pos.y - 1);
        }

        if (m_curBoard[pos.x + 1, pos.y] < minStep && m_curBoard[pos.x + 1, pos.y] > 0)
        {
            minStep = m_curBoard[pos.x + 1, pos.y];
            step = new Vector2Int(pos.x, pos.y - 1);
        }

        return step;
    }
}